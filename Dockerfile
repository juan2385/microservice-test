FROM openjdk:17-alpine

COPY target/microservice-test*.jar /microservice-test.jar

CMD ["java", "-jar", "/microservice-test.jar"]