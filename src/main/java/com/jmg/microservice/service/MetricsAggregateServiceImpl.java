package com.jmg.microservice.service;

import static com.jmg.microservice.service.MetricsAggregateUtils.averageMetrics;
import static com.jmg.microservice.service.MetricsAggregateUtils.averageTime;

import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jmg.microservice.client.MetricsClient;

import retrofit2.Response;

@Service
public class MetricsAggregateServiceImpl implements MetricsAggregateService {

	private static Logger log = LoggerFactory.getLogger(MetricsAggregateServiceImpl.class);

	@Autowired
	private MetricsClient client;

	@Override
	public Optional<JsonObject> getMetrics() {

		Response<JsonElement> serviceResponse;
		try {
			serviceResponse = client.getService().getAssetMetrics().execute();
			if (!serviceResponse.isSuccessful()) {
				return Optional.empty();
			}
		} catch (IOException e) {
			return Optional.empty();
		}

		return Optional.of(serviceResponse.body().getAsJsonObject());
	}

	@Override
	public JsonObject aggregate(JsonObject rootObject, int period) {
		JsonObject assetsResponse = new JsonObject();

		rootObject.entrySet().forEach(assetEntry -> {

			JsonObject metricsResponse = new JsonObject();

			JsonObject assetObject = assetEntry.getValue().getAsJsonObject();

			assetObject.entrySet().forEach(metricEntry -> {

				String metricId = metricEntry.getKey();
				JsonArray values = metricEntry.getValue().getAsJsonArray();
				JsonArray aggregated = !metricId.equals("time") ? averageMetrics(values, period)
						: averageTime(values, period);
				metricsResponse.add(metricId, aggregated);

			});

			String assetId = assetEntry.getKey();
			assetsResponse.add(assetId, metricsResponse);

		});

		if (log.isDebugEnabled()) {
			log.debug(assetsResponse.toString());
		}

		return assetsResponse;
	}

}
