package com.jmg.microservice.service;

import java.util.Optional;

import com.google.gson.JsonObject;

public interface MetricsAggregateService {
	
	JsonObject aggregate(JsonObject root, int aggregatePeriod);

	Optional<JsonObject> getMetrics();

}
