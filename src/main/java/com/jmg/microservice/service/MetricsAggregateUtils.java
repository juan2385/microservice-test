package com.jmg.microservice.service;

import static java.util.stream.Collectors.averagingDouble;

import java.util.stream.IntStream;

import com.google.gson.JsonArray;

public class MetricsAggregateUtils {
	
	public static JsonArray averageMetrics(JsonArray arr, int partition) {
		JsonArray result = new JsonArray();
		int parts = arr.size() / partition;
		for (int i = 0; i < parts; i++) {
			int initial = i * partition;
			Double aggAvg = IntStream.range(initial, initial + partition)
					.filter(idx -> !arr.get(idx).isJsonNull())
					.mapToObj(idx -> arr.get(idx).getAsDouble())
					.collect(averagingDouble(x -> x));
			result.add(aggAvg);
		}
		return result;
	}

	public static JsonArray averageTime(JsonArray arr, int partition) {
		JsonArray result = new JsonArray();
		int parts = arr.size() / partition;
		for (int i = 0; i < parts; i++) {
			int middle = i * partition + (partition / 2);
			result.add(arr.get(middle));
		}
		return result;
	}

}
