package com.jmg.microservice.rest.model;

public final class AggregateDto {

	private int period;

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

}
