package com.jmg.microservice.rest.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.jmg.microservice.rest.model.AggregateDto;
import com.jmg.microservice.service.MetricsAggregateService;

@RestController
@RequestMapping(value = "/metrics", produces = "application/json")
public class MetricsAggregateRestController {

	@Autowired
	private MetricsAggregateService metricsAggregateService;

	@PostMapping("/aggregate")
	public ResponseEntity<JsonObject> aggregate(@RequestBody(required = true) AggregateDto dto) {

		if (dto.getPeriod() != 10 && dto.getPeriod() != 30 && dto.getPeriod() != 60) {
			return ResponseEntity.badRequest().body(jsonMsg("Validation error", "Period values allowed: 10,30,60"));
		}

		Optional<JsonObject> rootObject = metricsAggregateService.getMetrics();
		if (!rootObject.isPresent()) {
			return ResponseEntity.internalServerError().body(jsonMsg("Error", "Error getting test data"));
		}

		JsonObject result = this.metricsAggregateService.aggregate(rootObject.get(), dto.getPeriod());
		return ResponseEntity.ok(result);
	}

	private JsonObject jsonMsg(String property, String value) {
		JsonObject obj = new JsonObject();
		obj.addProperty(property, value);
		return obj;
	}

}
