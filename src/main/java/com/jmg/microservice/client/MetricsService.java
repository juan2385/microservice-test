package com.jmg.microservice.client;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MetricsService {

	@GET("test.dataprovider")
	Call<JsonElement> getAssetMetrics();
}
