package com.jmg.microservice.client;

import org.springframework.stereotype.Component;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Component
public class MetricsClient {

	private static String BASE_URL = "https://reference.intellisense.io";

	private MetricsService service;

	public MetricsClient() {
		Retrofit client = createClient(BASE_URL);
		service = client.create(MetricsService.class);
	}

	public static Retrofit createClient(String apiURL) {
		return new Retrofit.Builder()
				.baseUrl(apiURL)
				.addConverterFactory(GsonConverterFactory.create()).build();
	}

	public MetricsService getService() {
		return service;
	}

}
