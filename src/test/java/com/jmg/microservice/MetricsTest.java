package com.jmg.microservice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map.Entry;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ClassPathResource;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.jmg.microservice.service.MetricsAggregateService;
import com.jmg.microservice.service.MetricsAggregateServiceImpl;
import com.jmg.microservice.service.MetricsAggregateUtils;

@SpringBootTest
@ComponentScan({ "com.jmg" })
class MetricsTest {

	private static Logger log = LoggerFactory.getLogger(MetricsAggregateServiceImpl.class);

	private static int DEFAULT_AGGREGATE_PERIOD = 60;

	@Autowired
	MetricsAggregateService service;

	@Test
	void aggregationServiceIntegrationTest() throws IOException {
		File resource = new ClassPathResource("test.json").getFile();
		String json = new String(Files.readAllBytes(resource.toPath()));
		JsonElement root = new Gson().fromJson(json, JsonElement.class);
		JsonElement response = service.aggregate(root.getAsJsonObject(), DEFAULT_AGGREGATE_PERIOD);
		log.debug("ROOT: " + root.toString());
		log.debug("Response: " + response.toString());

		for (Entry<String, JsonElement> entries : response.getAsJsonObject().entrySet()) {
			for (Entry<String, JsonElement> metrics : entries.getValue().getAsJsonObject().entrySet()) {
				assertEquals(3, metrics.getValue().getAsJsonArray().size());
			}
		}
	}

	@Test
	public void averageMetricsUnitTest() {
		JsonArray data = new JsonArray();
		data.add("1");
		data.add("2");
		data.add("3");
		data.add("4");
		data.add("5");
		data.add("6");
		JsonArray result = MetricsAggregateUtils.averageMetrics(data, 2);

		assertEquals(3, result.size());
		assertEquals("[1.5,3.5,5.5]", result.toString());
	}

	@Test
	public void averageTimeUnitTest() {
		JsonArray data = new JsonArray();
		data.add("2022-02-09T08:01:00.000Z");
		data.add("2022-02-09T08:02:00.000Z");
		data.add("2022-02-09T08:03:00.000Z");
		data.add("2022-02-09T08:04:00.000Z");
		data.add("2022-02-09T08:05:00.000Z");
		data.add("2022-02-09T08:06:00.000Z");
		JsonArray result = MetricsAggregateUtils.averageTime(data, 3);

		assertEquals(2, result.size());
		assertEquals("[\"2022-02-09T08:02:00.000Z\",\"2022-02-09T08:05:00.000Z\"]", result.toString());
	}

}
