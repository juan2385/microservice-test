# Instructions

### Steps to execute the application

* Pull Image from [docker hub](https://hub.docker.com/repository/docker/juan2385/microservice-test)
* Run docker container on port 8080
* Call the WS (Postman or other tool)

``` 
docker pull juan2385/microservice-test
docker run -d -p 8080:8080 juan2385/microservice-test
curl -X POST -H 'Content-Type: application/json' -i http://localhost:8080/metrics/aggregate --data '{"period":60}'
```
